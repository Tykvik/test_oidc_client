<%@ taglib prefix="bean" uri="http://struts.apache.org/tags-bean" %>
<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>post_auth_page</title>
</head>
<body>
<html:form action="/auth-callback">
    <p>authorize</p>
    <table border="1">
        <tr>
            <td>error</td><td><bean:write name="AuthCallbackForm" property="error" /></td>
        </tr>
        <tr>
            <td>error_description</td><td><bean:write name="AuthCallbackForm" property="error_description" /></td>
        </tr>
    </table>
</html:form>
</body>
</html>