<%@ taglib prefix="bean" uri="http://struts.apache.org/tags-bean" %>
<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>post_auth_page</title>
</head>
    <body>
        <html:form action="/auth-callback">
            <p>authorize</p>
            <table border="1">
                <tr>
                    <td>code</td><td><bean:write name="AuthCallbackForm" property="code" /></td>
                </tr>
                <tr>
                    <td>state</td><td><bean:write name="AuthCallbackForm" property="state" /></td>
                </tr>
            </table>
            <p>acess_token</p>
            <table border="1">
                <tr>
                    <td>acess_token</td><td><bean:write name="AuthCallbackForm" property="access_token"/> </td>
                </tr>
                <tr>
                    <td>expires</td><td><bean:write name="AuthCallbackForm" property="access_token_expires"/> </td>
                </tr>
            </table>
            <p>id_token</p>
            <table border="1">
                <tr>
                    <td>iss</td><td><bean:write name="AuthCallbackForm" property="iss"/> </td>
                </tr><tr>
                    <td>sub</td><td><bean:write name="AuthCallbackForm" property="sub"/> </td>
                </tr><tr>
                    <td>aud</td><td><bean:write name="AuthCallbackForm" property="aud"/> </td>
                </tr><tr>
                    <td>exp</td><td><bean:write name="AuthCallbackForm" property="exp"/> </td>
                </tr><tr>
                    <td>iat</td><td><bean:write name="AuthCallbackForm" property="iat"/> </td>
                </tr>
            </table>
            <p>userinfo</p>
            <table border="1">
                <tr>
                    <td>userinfo</td><td><bean:write name="AuthCallbackForm" property="userinfo"/> </td>
                </tr>
            </table>
        </html:form>
    </body>
</html>