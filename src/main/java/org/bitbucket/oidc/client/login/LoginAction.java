package org.bitbucket.oidc.client.login;

import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.common.message.types.ResponseType;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.bitbucket.oidc.client.utils.Nonce;
import org.bitbucket.oidc.client.utils.State;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.bitbucket.oidc.client.utils.Constants.*;

/**
 * Author:  helloween
 * Created: 06.03.17
 */
public class LoginAction extends Action {
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OAuthClientRequest authorizeRequest = OAuthClientRequest.authorizationLocation(AUTH_SERVICE_AUTHORIZE_URL)
                                              .setClientId(CLIENT_ID)
                                              .setRedirectURI(REDIRECT_URI)
                                              .setResponseType(ResponseType.CODE.toString())
                                              .setScope(OIDC_SCOPE)
                                              .setState(new State().getValue())
                                              .setParameter("nonce", new Nonce().getValue())
                                              .setParameter("client_secret", CLIENT_SECRET)
                                              .buildQueryMessage();
        ActionForward redirect = new ActionForward();
        redirect.setPath(authorizeRequest.getLocationUri());
        redirect.setRedirect(true);
        return redirect;
    }
}
