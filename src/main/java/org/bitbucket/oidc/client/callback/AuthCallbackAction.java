package org.bitbucket.oidc.client.callback;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.oltu.jose.jws.JWS;
import org.apache.oltu.jose.jws.io.JWSReader;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthAuthzResponse;
import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
import org.apache.oltu.oauth2.client.response.OAuthResourceResponse;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.apache.oltu.oauth2.common.utils.JSONUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.bitbucket.oidc.client.utils.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.StringTokenizer;

import static org.bitbucket.oidc.client.utils.Constants.*;

/**
 * Author:  helloween
 * Created: 07.03.17
 */
public class AuthCallbackAction extends Action {
    private static final Log log = LogFactory.getLog(AuthCallbackAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        AuthCallbackForm authCallbackForm = (AuthCallbackForm)form;

        if (!StringUtils.isEmpty(authCallbackForm.getError()))
            return mapping.findForward("failure");

        OAuthAuthzResponse authResponse = OAuthAuthzResponse.oauthCodeAuthzResponse(request);
        log.info("authcode:" + authResponse.getCode());
        log.info("state:" + authResponse.getState());
        log.info("Формируем запрос access_token");
        OAuthClientRequest accessTokenRequest = OAuthClientRequest.tokenLocation(AUTH_SERVICE_TOKEN_URL)
                                                       .setClientId(CLIENT_ID)
                                                       .setClientSecret(CLIENT_SECRET)
                                                       .setCode(authResponse.getCode())
                                                       .setGrantType(GrantType.AUTHORIZATION_CODE)
                                                       .setRedirectURI(REDIRECT_URI)
                                                       .buildBodyMessage();
        OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());
        log.info("Запрашиваем access_token");
        OAuthJSONAccessTokenResponse accessTokenResponse = oAuthClient.accessToken(accessTokenRequest, "POST");
        OIDCJSONAcessTokenResponse oidcjsonAcessTokenResponse = new OIDCJSONAcessTokenResponse(accessTokenResponse);
        String accessToken = oidcjsonAcessTokenResponse.getAccessToken();
        Long expiresIn  = oidcjsonAcessTokenResponse.getExpiresIn();
        String idToken = oidcjsonAcessTokenResponse.getIdToken();
        log.info("Парсим id_token");
        JWS jwtToken = new JWSReader().read(idToken);
        IDToken idTokenValue = IDTokenImpl.parse(jwtToken.getPayload());

        OAuthClientRequest userInfoRequest = new OIDCUserInfoClientRequest(Constants.AUTH_SERVICE_USERINFO_URL);
        userInfoRequest.addHeader("Authorization", "Bearer " + accessToken);
        OAuthResourceResponse uiResp = oAuthClient.resource(userInfoRequest, "GET", OAuthResourceResponse.class);

        authCallbackForm.setAccess_token(accessToken);
        authCallbackForm.setAccess_token_expires(expiresIn.toString());
        authCallbackForm.setIss(idTokenValue.iss());
        authCallbackForm.setSub(idTokenValue.sub());
        authCallbackForm.setAud(idTokenValue.aud());
        authCallbackForm.setExp(idTokenValue.exp());
        authCallbackForm.setIat(idTokenValue.iat());
        authCallbackForm.setUserinfo(uiResp.getBody());
        return mapping.findForward("success");
    }
}
