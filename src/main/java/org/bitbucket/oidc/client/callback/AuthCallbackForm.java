package org.bitbucket.oidc.client.callback;

import org.apache.struts.action.ActionForm;

/**
 * Author:  helloween
 * Created: 07.03.17
 */
public class AuthCallbackForm extends ActionForm {
    // errors
    private String error;
    private String error_description;

    // auth_code
    private String code;
    private String state;

    // tokens
    private String access_token;
    private String access_token_expires;
    // id_token_params
    private String iss;
    private String sub;
    private String aud;
    private String exp;
    private String iat;

    // userinfo
    private String userinfo;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCode() {

        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getIss() {
        return iss;
    }

    public void setIss(String iss) {
        this.iss = iss;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getAud() {
        return aud;
    }

    public void setAud(String aud) {
        this.aud = aud;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(Integer exp) {
        this.exp = exp.toString();
    }

    public String getIat() {
        return iat;
    }

    public void setIat(Integer iat) {
        this.iat = iat.toString();
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    public String getAccess_token_expires() {
        return access_token_expires;
    }

    public void setAccess_token_expires(String access_token_expires) {
        this.access_token_expires = access_token_expires;
    }

    public String getUserinfo() {
        return userinfo;
    }

    public void setUserinfo(String userinfo) {
        this.userinfo = userinfo;
    }
}
