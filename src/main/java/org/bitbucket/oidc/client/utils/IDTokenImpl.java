package org.bitbucket.oidc.client.utils;

import org.apache.oltu.oauth2.common.utils.JSONUtils;

import java.util.Map;

/**
 * Author:  helloween
 * Created: 07.03.17
 */
public class IDTokenImpl implements IDToken {
    private final String    iss;
    private final String    sub;
    private final String    aud;
    private final Integer   exp;
    private final Integer   iat;

    public IDTokenImpl(final String iss, final String sub, final String aud, final Integer exp, final Integer iat) {
        this.iss = iss;
        this.sub = sub;
        this.aud = aud;
        this.exp = exp;
        this.iat = iat;
    }

    @Override
    public String iss() {
        return iss;
    }

    @Override
    public String sub() {
        return sub;
    }

    @Override
    public String aud() {
        return aud;
    }

    @Override
    public Integer exp() {
        return exp;
    }

    @Override
    public Integer iat() {
        return iat;
    }

    public static IDToken parse(final String value) {
        final Map<String, Object> params = JSONUtils.parseJSON(value);

        return new IDTokenImpl((String)params.get("iss"),
                               (String)params.get("sub"),
                               (String)params.get("aud"),
                               (Integer)params.get("exp"),
                               (Integer)params.get("iat"));
    }
}
