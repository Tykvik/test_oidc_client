package org.bitbucket.oidc.client.utils;

import org.apache.oltu.oauth2.client.request.OAuthClientRequest;

/**
 * Author:  helloween
 * Created: 08.03.17
 */
public class OIDCUserInfoClientRequest extends OAuthClientRequest {

    public OIDCUserInfoClientRequest(String url) {
        super(url);
    }
}
