package org.bitbucket.oidc.client.utils;


import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;

/**
 * Author:  helloween
 * Created: 07.03.17
 */
public class Base64URL {
    private static final String CHARSET = "UTF-8";

    private final String value;

    public Base64URL(final String value) {
        this.value = value;
    }

    public byte[] decode() {
        return Base64.decodeBase64(value);
    }

    public String decodeToString() {
        try {
            return new String(decode(), CHARSET);
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    @Override
    public String toString() {
        return value;
    }

    public static Base64URL encode(final String value) {
        try {
            return encode(value.getBytes(CHARSET));
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public static Base64URL encode(final byte[] value) {
        return new Base64URL(Base64.encodeBase64URLSafeString(value));
    }
}
