package org.bitbucket.oidc.client.utils;

import org.apache.commons.lang.StringUtils;

/**
 * Author:  helloween
 * Created: 07.03.17
 */
public class Nonce extends Identifier {

    public Nonce(String value) {
        super(value);
    }

    public Nonce(int byteLength) {
        super(byteLength);
    }

    public Nonce() {
        super();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Nonce && this.toString().equals(o.toString());
    }

    public static Nonce parse(final String value) {
        if (StringUtils.isEmpty(value))
            return null;
        return new Nonce(value);
    }
}
