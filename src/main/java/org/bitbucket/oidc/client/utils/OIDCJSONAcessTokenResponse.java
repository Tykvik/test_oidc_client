package org.bitbucket.oidc.client.utils;

import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
import org.apache.oltu.oauth2.common.token.OAuthToken;

/**
 * Author:  helloween
 * Created: 07.03.17
 */
public class OIDCJSONAcessTokenResponse {
    private final OAuthJSONAccessTokenResponse response;

    public OIDCJSONAcessTokenResponse(OAuthJSONAccessTokenResponse response) {
        this.response = response;
    }

    public String getIdToken() {
        return response.getParam("id_token");
    }

    public String getAccessToken() {
        return response.getAccessToken();
    }

    public String getTokenType() {
        return response.getTokenType();
    }

    public Long getExpiresIn() {
        return response.getExpiresIn();
    }

    public String getScope() {
        return response.getScope();
    }

    public OAuthToken getOAuthToken() {
        return response.getOAuthToken();
    }

    public String getRefreshToken() {
        return response.getRefreshToken();
    }
}
