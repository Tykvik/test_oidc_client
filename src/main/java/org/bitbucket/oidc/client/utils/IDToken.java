package org.bitbucket.oidc.client.utils;

/**
 * Author:  helloween
 * Created: 07.03.17
 */
public interface IDToken {
    String iss();
    String sub();
    String aud();
    Integer exp();
    Integer iat();
}
