package org.bitbucket.oidc.client.utils;

import org.apache.commons.lang.StringUtils;

/**
 * Author:  helloween
 * Created: 07.03.17
 */
public class State extends Identifier {

    public State(String value) {
        super(value);
    }

    public State(int byteLength) {
        super(byteLength);
    }

    public State() {
        super();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof State && this.toString().equals(o.toString());
    }

    public static State parse(final String value) {
        if (StringUtils.isEmpty(value))
            return null;
        return new State(value);
    }
}
