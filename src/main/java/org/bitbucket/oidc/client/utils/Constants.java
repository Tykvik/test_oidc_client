package org.bitbucket.oidc.client.utils;

/**
 * Author:  helloween
 * Created: 06.03.17
 */
public interface Constants {
    String AUTH_SERVICE_AUTHORIZE_URL = "https://account.bioid.com/connect/authorize";
    String AUTH_SERVICE_TOKEN_URL     = "https://account.bioid.com/connect/token";
    String AUTH_SERVICE_USERINFO_URL  = "https://account.bioid.com/openid/userinfo";
    String CLIENT_ID                  = "c8706f43-3d12-40c6-9542-e11f12768087";
    String CLIENT_SECRET              = "fFHYIH84QJgHO3Ni5q+K9Fv1";
    String REDIRECT_URI               = "http://localhost:8080/test.oidc.client/auth-callback.do";
    String OIDC_SCOPE                 = "openid";
}
