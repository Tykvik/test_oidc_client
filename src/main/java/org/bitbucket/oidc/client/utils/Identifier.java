package org.bitbucket.oidc.client.utils;

import org.apache.commons.lang.StringUtils;

import java.security.SecureRandom;

/**
 * Author:  helloween
 * Created: 07.03.17
 */
public class Identifier implements Comparable<Identifier> {
    private static final int DEFAULT_BYTE_LENGTH = 32;
    private static final SecureRandom secureRandom = new SecureRandom();

    private final String value;

    public Identifier(final String value) {

        if (StringUtils.isEmpty(value))
            throw new IllegalArgumentException("Значение не должно быть null или пустой строкой");

        this.value = value;
    }


    public Identifier(final int byteLength) {

        if (byteLength < 1)
            throw new IllegalArgumentException("Длина в байтах должна быть больше 0");

        byte[] n = new byte[byteLength];

        secureRandom.nextBytes(n);

        value = Base64URL.encode(n).toString();
    }


    public Identifier() {

        this(DEFAULT_BYTE_LENGTH);
    }


    public String getValue() {

        return value;
    }


    @Override
    public String toString() {

        return getValue();
    }

    @Override
    public int compareTo(final Identifier other) {

        return getValue().compareTo(other.getValue());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Identifier that = (Identifier) o;

        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }
}
